# equivalence.vim

Automatically substitute the corresponding equivalence classes for alphabetic characters in searches.  This allows searches for e.g., the surname "Ngo" to match instances with accented characters "Ngô".

## Installation

1.  Add the repository's Git URL (`https://gitlab.com/lafrenierejm/vim-equivalence.git`) to your favorite plugin manager's list of plugins.

    Using [vim-plug](https://github.com/junegunn/vim-plug), this would be:
    ```
Plug 'https://gitlab.com/lafrenierejm/vim-equivalence.git'
    ```
2.  Add keybindings to your init file for the commands in this plugin's `plugins` directory.

    Example keybindings:
    ```vim
" Search equivalence forward for current selection
vnoremap <leader>/ :VisualEquivalenceForward<CR>
" Search equivalence backward for current selection
vnoremap <leader>? :VisualEquivalenceBackward<CR>
    ```

## License

Copyright © Joseph LaFreniere <joseph@lafreniere.xyz>.  Distributed under the permissive [ISC License](https://gitlab.com/lafrenierejm/vim-equivalence/raw/master/LICENSE).

## Credit

The function for retrieving the current text selection was written by [xolox as an answer](http://stackoverflow.com/a/6271254) to the StackOverflow post "How to get visually selected text in VimScript".
