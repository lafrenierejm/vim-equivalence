" Use UTF-8 encoding for this script
scriptencoding utf-8

" Return the argument text with each alphabetic character replaced with its
" equivalence class.
function! s:convert_to_equivalence(text) abort
	return substitute(a:text, '\a', '[[=\0=]]', 'g')
endfunction

" Return the currently selected text.
" Credit to xolox (http://stackoverflow.com/a/6271254)
function! equivalence#get_visual_selection() abort
	let [l:lnum1, l:col1] = getpos("'<")[1:2]
	let [l:lnum2, l:col2] = getpos("'>")[1:2]
	let l:lines = getline(l:lnum1, l:lnum2)
	let l:lines[-1] = l:lines[-1][: l:col2 - (&selection == 'inclusive' ? 1 : 2)]
	let l:lines[0] = l:lines[0][l:col1 - 1:]
	return join(l:lines, "\n")
endfunction

" Perform the actual search for the equivalence text.
function! equivalence#search_equivalence(text, search_forward) abort
	" Get the the equivalence of `a:text`.
	let l:equivalence = s:convert_to_equivalence(a:text)
	" Add `l:equivalence` to the search history.
	call histadd('search', l:equivalence)
	" Set the current search term to `l:equivalence`.
	let @/ = l:equivalence
	" Determine if the search is forward or backward.
	if a:search_forward
		" Search forward for l:equivalence starting at the cursor position.
		call search(l:equivalence, 'z')
	else
		" Search backward for `l:equivalence` starting at the cursor position.
		call search(l:equivalence, 'bz')
	endif
endfunction
