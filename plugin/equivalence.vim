command! -range VisualEquivalenceForward call equivalence#search_equivalence(equivalence#get_visual_selection(), 1) | let v:searchforward = 1
command! -range VisualEquivalenceBackward call equivalence#search_equivalence(equivalence#get_visual_selection(), 0) | let v:searchforward = 0
